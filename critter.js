const Critter = () => ({ system, id }) => {
  const sex = Math.random() > 0.5 ? 'M' : 'F'
  const speed = Math.floor(Math.random() * 300) + 50

  const details = system.addActor(CritterDetails(id, sex))
  const visibleDetails = () => ({ sex })

  return composeHandlers(
    debug(log => {
      log('Critter:')
      log(' ', 'id:', id)
    }),
    isVisible('Critter', id, system, visibleDetails),
    hasVision(id, system),
    hasCurrentLocation(id, system),
    isMortal(id, system),
    hasEffects(id, system),
    hasDrives(id, system, [
      system.addActor(Hunger(id, 500, speed)),
      system.addActor(Exhaustion(id, 1000)),
    ]),
    cleanup(() => { system.removeActor(details) }),
  )
}

const CritterDetails = (parentId, sex) => ({ system, id, gui }) => {
  let effects = new Set()

  updateGui = displayInGui(gui, parentId, 'Critter', () => ({
    sex,
    effects
  }))

  return composeHandlers(
    subscribeTo('EFFECTS', id, parentId, system, newEffects => {
      effects = newEffects
      updateGui()
    }),
  )
}
