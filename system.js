function IdGenerator() {
  let nextId = 1

  return function() {
    const id = nextId
    nextId++
    return id
  }
}

function System(createGui, createWorld, keepStats) {
  const actors = {}
  const assignId = IdGenerator()
  let gui, world

  const tickTimes = {}

  function send(recipient, msg) {
    if(!actors[recipient]) return
    actors[recipient].forEach(actor => actor(msg))
  }

  const actionsAvailableToActors = {
    send,
    addActor(makeActor) {
      const id = assignId()
      actors[id] = [makeActor({ system: actionsAvailableToActors, id, gui, world })]
      return id
    },
    removeActor(id) {
      send(id, { type: 'CLEANUP' })
      delete actors[id]
    }
  }

  gui = actionsAvailableToActors.addActor(createGui)
  world = actionsAvailableToActors.addActor(createWorld)

  return {
    ...actionsAvailableToActors,
    world,
    gui,
    tick() {
      for(recipient in actors) {
        if(keepStats) {
          if(!tickTimes[recipient]) tickTimes[recipient] = { longest: 0, total: 0, ticks: 0 }
          const start = new Date()
          send(recipient, { type: 'TICK' })
          const end = new Date()
          const time = end - start
          tickTimes[recipient].total += time
          tickTimes[recipient].longest = Math.max(tickTimes[recipient].longest, time)
          tickTimes[recipient].ticks++
          tickTimes[recipient].average = tickTimes[recipient].total / tickTimes[recipient].ticks
        } else {
          send(recipient, { type: 'TICK' })
        }
      }
    },
    stats(id) {
      if(id) {
        console.log('stats for',id)
        console.log(tickTimes[id])
      } else {
        const slowIds = Object.keys(tickTimes).reduce((list, nextId) => {
          if(list.length < 10) {
            list.push(nextId)
            return list
          }

          if(list.every(slowId => tickTimes[slowId].average > tickTimes[nextId].average)) {
            return list
          } else {
            list = list.filter(slowId => tickTimes[slowId].average > tickTimes[nextId].average)
            list.push(nextId)
            return list
          }
        }, [])

        slowIds.forEach(slowId => console.log(slowId, tickTimes[slowId]))
      }
    }
  }
}
