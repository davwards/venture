function mergeSenseData(original, update) {
  if(typeof update === 'object') {
    const result = original || {}
    for(key in update) {
      result[key] = mergeSenseData(result[key], update[key])
    }
    return result
  } else {
    return update
  }
}

function removeSenseData(original, update) {
  if(typeof update === 'object') {
    for(key in update) {
      if(original[key]) {
        original[key] = removeSenseData(original[key], update[key])
      }
    }
  } else {
    delete original[update]
  }

  return original
}

const Space = (position) => ({ system, id, world, gui }) => {
  let contents = {}
  let senses = {}
  let senseData = {}

  const spaceDetails = () => ({})

  return handle({
    PLACE_ACTOR: (msg) => {
      contents[msg.actor] = msg.details || {}
      system.send(msg.actor, {
        type: 'NEW_LOCATION',
        space: id,
        details: spaceDetails(),
        position,
      })
      for(actorId in contents) {
        if(actorId !== msg.actor) {
          system.send(actorId, {
            type: 'NEW_NEIGHBOR',
            actor: msg.actor,
          })
        }
      }
      system.send(gui, msg)
    },

    VACATE: ({ actor }) => {
      delete contents[actor]
      for(sense in senses) senses[sense].delete(actor)

      system.send(gui, {
        type: 'REMOVE_ACTOR',
        actor,
        position,
      })
    },

    TELL_NEIGHBORS: ({ actor, message }) => {
      for(neighbor in contents) {
        if(neighbor !== actor) system.send(neighbor, message)
      }
    },

    UPDATE_SENSE_DATA: ({ sense, data }) => {
      if(!senseData[sense]) senseData[sense] = {}
      senseData[sense] = mergeSenseData(senseData[sense], data)
      if(senses[sense]) {
        senses[sense].forEach(sensor => {
          system.send(sensor, {
            type: 'UPDATE_SENSE_DATA',
            sense,
            position,
            data: senseData[sense],
          })
        })
      }
    },

    REMOVE_SENSE_DATA: ({ sense, update }) => {
      if(!senseData[sense]) return
      senseData[sense] = removeSenseData(senseData[sense], update)
      if(senses[sense]) {
        senses[sense].forEach(sensor => {
          system.send(sensor, {
            type: 'UPDATE_SENSE_DATA',
            sense,
            position,
            data: senseData[sense],
          })
        })
      }
    },

    REGISTER_SENSE: ({ actor, sense }) => {
      if(!senses[sense]) senses[sense] = new Set()
      senses[sense].add(actor)
      if(senseData[sense]) {
        system.send(actor, {
          type: 'UPDATE_SENSE_DATA',
          sense,
          position,
          data: senseData[sense]
        })
      }
    },

    DEBUG: () => {
      console.log('Space at', position)
      console.log('contents:', contents)
      console.log('sensors:', senses)
      console.log('sense data:', senseData)
    }
  })
}

const World = () => ({ system, id, gui }) => {
  const spaces = {}

  return handle({
    PLACE_ACTOR: (msg) => {
      const { position } = msg

      if(!spaces[position]) {
        spaces[position] = system.addActor(Space(position))
        const [x,y] = coords(position)
      }

      system.send(spaces[position], msg)
    },

    DEBUG: () => console.log(spaces)
  })
}
