function isVisible(actorType, actor, system, getDetails) {
  let currentPosition = null
  let light = null

  const fullDetails = () => ({
    ...(getDetails ? getDetails() : null),
    actor,
    actorType,
  })

  return {
    NEW_LOCATION: ({ space, position }) => {
      if(light && currentPosition && position !== currentPosition) {
        system.send(light, { type: 'LOST_SOURCE' })
      }

      light = system.addActor(Light(
        fullDetails(),
        actor,
        space,
        position,
      ))

      system.send(space, {
        type: 'PLACE_ACTOR',
        position,
        actor: light,
      })

      currentPosition = position
    },

    ...debug(log => {
      log(' ','isVisible:')
      log('   ','id:',actor)
      log('   ','light:',light)
      log('   ','from:',currentPosition)
      log('   ','visible details:',fullDetails())
    }),

    CLEANUP: () => {
      if(light) system.send(light, { type: 'LOST_SOURCE' })
    }
  }
}

function hasVision(id, system, reportChange = () => {}) {
  let fieldOfView = {}
  const [updateSubscriptions, subscriptionHandlers] =
    isSubscribable('VISION', () => fieldOfView, id, system)

  return {
    UPDATE_SENSE_DATA: ({ sense, data }) => {
      if(sense !== 'vision') return
      fieldOfView = data
      reportChange(fieldOfView)
      updateSubscriptions()
    },

    NEW_LOCATION: ({ space }) => {
      system.send(space, {
        type: 'REGISTER_SENSE',
        actor: id,
        sense: 'vision',
      })
    },

    ...debug(log => {
      log(' ','hasVision:')
      log('   ','id:',id)
      log('   ','field of view:',fieldOfView)
    }),

    ...subscriptionHandlers,
  }
}

const Light = (image, actorId, sourceId, sourcePosition) => ({ system, id, world, gui }) => {
  const illuminatedPositions = { [sourcePosition]: sourceId }

  // maintaining a count of illuminatedPositions
  // instead of using Object.keys(illuminatedPositions).length
  // has a significant performance impact
  let numberOfIlluminatedPositions = 1

  const [sx, sy] = coords(sourcePosition)
  let illuminatedFrontier = new Set(Object.keys(illuminatedPositions))
  let shadowFrontier = new Set()
  let lostSource = false

  function revealImageTo(position) {
    system.send(illuminatedPositions[position], {
      type: 'UPDATE_SENSE_DATA',
      sense: 'vision',
      data: {
        [sourcePosition]: {
          [actorId]: image
        }
      }
    })
  }

  function obscureImageAt(position) {
    system.send(illuminatedPositions[position], {
      type: 'REMOVE_SENSE_DATA',
      sense: 'vision',
      update: { [sourcePosition]: actorId }
    })
  }

  function neighborsToSpreadTo(position) {
    return lightSpread1(sx,sy,position)
  }

  function propogate() {
    if(illuminatedFrontier.size > 0) {
      const newFrontier = new Set()
      illuminatedFrontier.forEach(position => {
        neighborsToSpreadTo(position).forEach(neighbor => {
          if(distance(sourcePosition, neighbor) > 20) return
          newFrontier.add(neighbor)
        })
      })
      illuminatedFrontier = newFrontier
      illuminatedFrontier.forEach(position => {
        if(illuminatedPositions[position]) {
          revealImageTo(position)
        } else {
          system.send(world, {
            type: 'PLACE_ACTOR',
            position,
            actor: id,
          })
        }
      })
    }

    if(shadowFrontier.size > 0) {
      const newFrontier = new Set()
      shadowFrontier.forEach(position => {
        neighborsToSpreadTo(position).forEach(neighbor => {
          if(!illuminatedPositions[neighbor]) return
          newFrontier.add(neighbor)
        })
      })
      shadowFrontier = newFrontier
      shadowFrontier.forEach(position => {
        obscureImageAt(position)
        system.send(illuminatedPositions[position], {
          type: 'VACATE',
          actor: id,
        })
        numberOfIlluminatedPositions--
        delete illuminatedPositions[position]
      })
    }
  }

  return handle({
    TICK: () => {
      propogate()
      if(
        illuminatedFrontier.size === 0
          && shadowFrontier.size === 0
          && numberOfIlluminatedPositions === 0
      ) {
        system.removeActor(id)
      }
    },

    LOST_SOURCE: () => {
      lostSource = true
      shadowFrontier.add(sourcePosition)
      obscureImageAt(sourcePosition)
      numberOfIlluminatedPositions--
      delete illuminatedPositions[sourcePosition]
      system.send(sourceId, {
        type: 'VACATE',
        actor: id,
      })
    },

    NEW_LOCATION: ({ space, position }) => {
      if(!illuminatedPositions[position]) numberOfIlluminatedPositions++
      illuminatedPositions[position] = space
      revealImageTo(position)
    },

    CLEANUP: () => {
      for(position in illuminatedPositions) {
        obscureImageAt(position)
        system.send(illuminatedPositions[position], {
          type: 'VACATE',
          actor: id,
        })
      }
    },

    DEBUG: ({ prefix }) => {
      console.log(prefix||'' + '', 'Light:')
      console.log(prefix||'' + '  ', 'id:', id)
      console.log(prefix||'' + '  ', 'source space:', sourceId)
      console.log(prefix||'' + '  ', 'source position:', sourcePosition)
      console.log(prefix||'' + '  ', 'illuminated frontier:', illuminatedFrontier)
      console.log(prefix||'' + '  ', 'shadow frontier:', shadowFrontier)
      console.log(prefix||'' + '  ', 'illuminated spaces:')
      console.log(prefix||'' + '    ', 'true count:', Object.keys(illuminatedPositions).length)
      console.log(prefix||'' + '    ', 'cached count:', numberOfIlluminatedPositions)
    },
  })
}
