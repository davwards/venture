# Venture

**What is this?**

A little browser-based simulation game, born from some thoughts I had while playing Dwarf Fortress.

**What's interesting about it?**

Not much yet. I'm using it to experiment with the actor model paradigm. Turns out, actors are fun!

**How do I play?**

Clone the repository, open `index.html` in your browser, and press the spacebar to start the simulation!

**I want to look through the code; where do I start?** 

`critter.js` is a good starting point. A lot of what's in it is quickly
recognizable from the simulation, and it references a bunch of things that might
catch your interest.

If you want to trace execution from the beginning, check the script tag in `index.html`.
