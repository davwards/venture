const Sleep = (parentId, length) => ({ system, id }) => {
  let timeToSleep = length

  function sleep() {
    system.send(parentId, {
      type: 'SLEEP'  
    })

    timeToSleep--

    if(timeToSleep <= 0) {
      awaken()
      system.removeActor(id)
    }
  }

  function awaken() {
    system.send(parentId, {
      type: 'WAKE_UP'  
    })
  }

  return handle({
    START_PROJECT: sleep,
    WORK_ON_PROJECT: sleep,
    ABORT_PROJECT: awaken,
    ...debug(log => {
      log('Sleep:', parentId, 'time to sleep', timeToSleep)
      log(' ', 'for:', parentId)
      log(' ', 'time to sleep:', timeToSleep)
    })
  })
}

const Exhaustion = (parentId, exhaustionThreshold) => ({ system, id }) => {
  let exhaustion = 0
  let sleep = null

  const priority = sigmoidPriorityCurve(exhaustionThreshold)

  return composeHandlers(
    debug((log, logActor) => {
      log('Exhaustion:')
      log(' ', 'for:', parentId)
      log(' ', 'exhaustion level:', exhaustion)
      log(' ', 'death at:', exhaustionThreshold)
      log(' ', 'sleep project:', sleep)
      if(sleep) logActor(system, sleep)
    }),
    {
      TICK: everyNTimes(20, () => {
        exhaustion++
        system.send(parentId, {
          type: 'UPDATE_PRIORITY',
          actor: id,
          priority: priority(exhaustion)
        })

        if(exhaustion >= exhaustionThreshold) {
          system.send(parentId, { type: 'DIE', cause: 'exhausation' })
          system.removeActor(id)
          return
        }
      }),

      ACT: () => {
        if(sleep === null) {
          sleep = system.addActor(Sleep(id, Math.max(exhaustion - 100, 200)))
          system.send(parentId, {
            type: 'START_PROJECT',
            project: sleep,
            drive: id,
          })
        } else {
          system.send(parentId, {
            type: 'WORK_ON_PROJECT',
            project: sleep,
            drive: id,
          })
        }
        exhaustion -= 500
      },

      SLEEP: () => {
        exhaustion--
        system.send(parentId, {
          type: 'ADD_EFFECT',
          effect: 'sleeping'
        })
      },

      WAKE_UP: () => {
        system.send(parentId, {
          type: 'REMOVE_EFFECT',
          effect: 'sleeping'
        })
        if(sleep !== null) {
          system.send(parentId, {
            type: 'STOP_PROJECT',
            project: sleep,
          })
          system.removeActor(sleep)
          sleep = null
        }
      },
    }
  )
}
