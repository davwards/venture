const KeyMapping = (doc, metronome, tick) => ({ system, id, gui }) => {
  let actionMultiplier = ''

  const actions = {
    PAN_VIEWPORT_SOUTH: () => { system.send(gui, {type: 'PAN_VIEWPORT_SOUTH'}) },
    PAN_VIEWPORT_NORTH: () => { system.send(gui, {type: 'PAN_VIEWPORT_NORTH'}) },
    PAN_VIEWPORT_WEST: () => { system.send(gui, {type: 'PAN_VIEWPORT_WEST'}) },
    PAN_VIEWPORT_EAST: () => { system.send(gui, {type: 'PAN_VIEWPORT_EAST'}) },
    TOGGLE: () => { system.send(metronome, {type: 'TOGGLE'}) },
    TOGGLE_IDS: () => { system.send(gui, {type: 'TOGGLE_IDS'}) },
    TOGGLE_AXES: () => { system.send(gui, {type: 'TOGGLE_AXES'}) },
    TICK: () => { tick() },
  }

  const mapping = {
    'j': 'PAN_VIEWPORT_SOUTH',
    'k': 'PAN_VIEWPORT_NORTH',
    'h': 'PAN_VIEWPORT_WEST',
    'l': 'PAN_VIEWPORT_EAST',
    ' ': 'TOGGLE',
    '#': 'TOGGLE_IDS',
    '+': 'TOGGLE_AXES',
    't': 'TICK',
  }

  const handleKeydown = (event) => {
    if(/[0-9]/.test(event.key)) {
      actionMultiplier += event.key
      return
    }
    if(mapping[event.key]) {
      const timesToDoAction = parseInt(actionMultiplier || '1')
      for(let i=0; i<timesToDoAction; i++) actions[mapping[event.key]]()
      actionMultiplier = ''
    }
    else {
      console.log('Received unmapped key:', event.key)
      actionMultiplier = ''
    }
  }

  doc.addEventListener('keydown', handleKeydown)

  return handle({
    CLEANUP: () => { doc.removeEventListener('keydown', handleKeydown) }
  })
}

const Gui = (element) => ({ system, id }) => {
  const ctx = element.getContext("2d")
  ctx.font = '18px monospace'
  const VIEWPORT_WIDTH = 52
  const VIEWPORT_HEIGHT = 48
  const CANVAS_WIDTH = element.width
  const CANVAS_HEIGHT = canvas.height

  let viewportCenterX = 0
  let viewportCenterY = 0
  let tickCount = 0
  let showIds = false
  let showAxes = false
  let tps = 0

  let upToDate = false
  const invalidateCache = () => upToDate = false

  const positions = {}

  const actorDetails = {}

  const canvasCoordsFor = (position) => {
    const [x, y] = coords(position)
    return [
      CANVAS_WIDTH / 2 + (x - viewportCenterX) * 18 + 18,
      CANVAS_HEIGHT / 2 + (y - viewportCenterY) * 18 + 36
    ]
  }

  const viewportWestEdge = () => Math.floor(viewportCenterX - VIEWPORT_WIDTH/2)
  const viewportEastEdge = () => Math.floor(viewportCenterX + VIEWPORT_WIDTH/2)
  const viewportNorthEdge = () => Math.floor(viewportCenterY - VIEWPORT_HEIGHT/2)
  const viewportSouthEdge = () => Math.floor(viewportCenterY + VIEWPORT_HEIGHT/2)

  function renderActor(actor, canvasX, canvasY) {
    if(!actorDetails[actor]) return
    const renderer = renderers[actorDetails[actor].actorType]
    if(!renderer) return
    const { color, glyph } = renderer(actorDetails[actor].details)
    ctx.fillStyle = color
    ctx.fillText(
      glyph + (showIds ? actor : ''),
      canvasX,
      canvasY
    )
  }

  function redraw() {
    if(upToDate) return
    ctx.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT)

    for(let x = viewportWestEdge(); x <= viewportEastEdge(); x++) {
      for(let y = viewportNorthEdge(); y <= viewportSouthEdge(); y++) {
        if(showAxes && x === viewportWestEdge()) {
          const [cx, cy] = canvasCoordsFor(`${x},${y}`)
          const [cx2, cy2] = canvasCoordsFor(`${viewportEastEdge()},${y}`)
          ctx.save()
          ctx.fillStyle = 'black'
          ctx.strokeStyle = '#DDF'
          ctx.textAlign = 'right'
          ctx.fillText(y, cx-12, cy)
          ctx.beginPath()
          ctx.moveTo(cx-12, cy - 7)
          ctx.lineTo(cx2+18, cy2 - 7)
          ctx.stroke()
          ctx.restore()
        }
        if(showAxes && y === viewportNorthEdge()) {
          const [cx, cy] = canvasCoordsFor(`${x},${y}`)
          const [cx2, cy2] = canvasCoordsFor(`${x},${viewportSouthEdge()}`)
          ctx.save()
          ctx.fillStyle = 'black'
          ctx.translate(cx, cy)
          ctx.rotate(Math.PI/2)
          ctx.textAlign = 'right'
          ctx.fillText(x, -18, 0)
          ctx.restore()
          ctx.save()
          ctx.strokeStyle = '#DDF'
          ctx.beginPath()
          ctx.moveTo(cx+6, cy-18)
          ctx.lineTo(cx2+6, cy2)
          ctx.stroke()
          ctx.restore()
        }
        const position = `${x},${y}`

        if(!positions[position]) continue

        const [canvasX, canvasY] = canvasCoordsFor(position)
        const drawableContents =
          positions[position].filter(item => !!actorDetails[item])

        if(drawableContents.length > 0) {
          const itemToDraw = drawableContents[
            Math.floor(tickCount/300) % drawableContents.length
          ]
          renderActor(itemToDraw, canvasX, canvasY)
        }
      }
    }

    ctx.fillStyle = 'black'
    ctx.fillText( tps + ' TPS', 10, 20)
    upToDate = true
  }

  return handle({
    PLACE_ACTOR: ({ position, actor }) => {
      if(!positions[position]) positions[position] = []
      positions[position].push(actor)
      if(actorDetails[actor]) invalidateCache()
    },

    REMOVE_ACTOR: ({ position, actor }) => {
      if(!positions[position]) positions[position] = []
      positions[position] = positions[position].filter(id => id !== actor)
      if(actorDetails[actor]) invalidateCache()
    },

    UPDATE_ACTOR: ({ actor, actorType, details }) => {
      actorDetails[actor] = { actorType, details }
      invalidateCache()
    },

    REDRAW: () => {
      tickCount = tickCount + 1 % 100000
      redraw()
    },

    PAN_VIEWPORT_WEST: () => {
      viewportCenterX--
      invalidateCache()
      redraw()
    },

    PAN_VIEWPORT_EAST: () => {
      viewportCenterX++
      invalidateCache()
      redraw()
    },

    PAN_VIEWPORT_NORTH: () => {
      viewportCenterY--
      invalidateCache()
      redraw()
    },

    PAN_VIEWPORT_SOUTH: () => {
      viewportCenterY++
      invalidateCache()
      redraw()
    },

    TOGGLE_IDS: () => {
      showIds = !showIds
      invalidateCache()
      redraw()
    },

    TOGGLE_AXES: () => {
      showAxes = !showAxes
      invalidateCache()
      redraw()
    },

    UPDATE_TPS: ({ ticks }) => {
      tps = ticks
      invalidateCache()
    }
  })
}

const renderers = {
  Critter: ({ sex, effects }) => {
    const color = sex === 'M' ? 'blue' : 'red'
    let glyph = sex === 'M' ? '♂' : '♀'

    if(effects.has('sleeping')) glyph = 'Z'

    return { color, glyph }
  },

  Food: () => ({ color: 'green', glyph: '*' }),

  Corpse: () => ({ color: 'black', glyph: '☠' }),
}

function displayInGui(gui, actor, actorType, getDetails = () => ({})) {
  const update = () => {
    system.send(gui, {
      type: 'UPDATE_ACTOR',
      actor,
      actorType,
      details: getDetails()
    })
  }

  update()

  return update
}

const Metronome = (tick) => ({ system, id, gui }) => {
  let runSimulation = false
  let ticksThisSecond = 0

  const tickInterval = setInterval(() => {
    if(runSimulation) {
      ticksThisSecond++
      tick()
      ticksThisSecond++
      tick()
      ticksThisSecond++
      tick()
    }
  }, 2)

  const guiInterval = setInterval(() => {
    system.send(gui, { type: 'REDRAW' })
  }, 250)

  const tpsMeterInterval = setInterval(() => {
    if(runSimulation) {
      system.send(gui, { type: 'UPDATE_TPS', ticks: ticksThisSecond })
      ticksThisSecond = 0
    }
  }, 1000)

  function report() { console.log('Simulation is', runSimulation ? 'running' : 'stopped') }

  return handle({
    PLAY: () => { runSimulation = true ; report() },
    PAUSE: () => { runSimulation = false ; report() },
    TOGGLE: () => { runSimulation = !runSimulation ; report() },
    CLEANUP: () => {
      clearInterval(tickInterval)
      clearInterval(tpsMeterInterval)
      clearInterval(guiInterval)
    }
  })
}
