function everyNTimes(n, work) {
  let count = 0

  return function(...args) {
    count++
    if(count % n === 0) work(...args)
  }
}

const handle = handlers => function(msg) {
  if(handlers[msg.type]) {
    handlers[msg.type](msg)
  }
}

const composeHandlers = (...handlerCollections) => {
  const handlers = handlerCollections.map(handle)
  return message => {
    for(i in handlers) handlers[i](message)
  }
}

function isSubscribable(subscriptionType, value, id, system) {
  const listeners = new Set()

  function notifyListener(listener) {
    system.send(listener, {
      type: `UPDATE::${subscriptionType}`,
      update: value(),
      actor: id,
    })
  }

  function notifyListeners() {
    listeners.forEach(notifyListener)
  }

  return [
    notifyListeners,
    {
      [`ADD_LISTENER::${subscriptionType}`]: ({ actor }) => {
        listeners.add(actor)
        notifyListener(actor)
      },

      [`REMOVE_LISTENER::${subscriptionType}`]: ({ actor }) => {
        listeners.delete(actor)
      },
    }
  ]
}

function subscribeTo(subscriptionType, id, target, system, callback) {
  let subscribed = false

  return {
    TICK: () => {
      if(!subscribed) {
        system.send(target, {
          type: `ADD_LISTENER::${subscriptionType}`,
          actor: id,
        })
        subscribed = true
      }
    },
    [`UPDATE::${subscriptionType}`]: ({ update, actor }) => {
      if(actor === target) callback(update)
    },
    CLEANUP: () => {
      system.send(target, {
        type: `REMOVE_LISTENER::${subscriptionType}`,
        actor: id,
      })
    }
  }
}

function hasCurrentLocation(id, system, reportChange = () => {}) {
  let currentLocation = {}
  const [updateSubscriptions, subscriptionHandlers] =
    isSubscribable('LOCATION', () => currentLocation, id, system)

  function vacateSpace(space) {
    system.send(space, {
      type: 'VACATE',
      actor: id
    })
  }

  return {
    NEW_LOCATION: ({ space, position }) => {
      if(currentLocation.space) vacateSpace(currentLocation.space)
      currentLocation = { space, position }
      reportChange(currentLocation)
      updateSubscriptions()
    },

    ...subscriptionHandlers,

    ...debug(log => {
      log(' ','hasCurrentLocation:', currentLocation)
    }),

    CLEANUP: () => {
      if(currentLocation.space) {
        system.send(currentLocation.space, {
          type: 'VACATE',
          actor: id
        })
      }
    }
  }
}

function isMobile(id, speed, system, world, currentLocation, reportFailure = () => {}) {
  const mobilityHandlers = {
    FAILED_TO_PLACE: ({ position }) => {
      reportFailure(position)
    }
  }

  return [
    everyNTimes(speed, (destination) => {
      if(!currentLocation()) return
      if(distance(currentLocation(), destination) > 1.5) {
        console.log('actor',id,'wants to move from',currentLocation(),'to',destination,'but it is too far!')
        reportFailure(destination)
        return
      }
      system.send(world, {
        type: 'PLACE_ACTOR',
        actor: id,
        position: destination,
        details: {},
      })
    }),
    mobilityHandlers
  ]
}

function hasDrives(id, system, drives) {
  const priorities = {}
  drives.forEach(drive => priorities[drive] = 0)

  let currentProject = null

  const topPriorityDrive = () =>
    Object
      .keys(priorities)
      .reduce((highest, drive) =>
        priorities[highest] > priorities[drive]
          ? highest
          : drive
      )

  return {
    UPDATE_PRIORITY: ({ actor, priority }) => {
      priorities[actor] = priority
    },

    TICK: () => {
      if(currentProject) {
        system.send(currentProject.id, {
          type: 'WORK_ON_PROJECT',
          actor: id,
        })
      } else {
        system.send(topPriorityDrive(), { type: 'ACT' })
      }
    },

    START_PROJECT: ({ project, drive }) => {
      currentProject = {
        id: project,
        drive: drive,
      }
      system.send(project, {
        type: 'START_PROJECT',
        actor: id,
      })
    },

    WORK_ON_PROJECT: ({ project }) => {
      system.send(project, {
        type: 'WORK_ON_PROJECT',
        actor: id,
      })
    },

    STOP_PROJECT: ({ project }) => {
      if(currentProject && currentProject.id === project) {
        currentProject = null
      }
    },

    CLEANUP: () => {
      if(currentProject) system.removeActor(currentProject.id)
      drives.forEach(system.removeActor)
    },

    ...debug((log, logActor) => {
      log(' ','hasDrives:')
      log('   ','drives:', drives)
      log('   ','priorities:', priorities)
      log('   ','currentProject:', currentProject || '(none)')
      drives.forEach(drive => logActor(system, drive))
    }),
  }
}

function hasEffects(id, system, reportChange = () => {}) {
  const effects = new Set()

  const [updateSubscriptions, subscriptionHandlers] =
    isSubscribable('EFFECTS', () => new Set(effects), id, system)

  return {
    ADD_EFFECT: ({ effect }) => {
      effects.add(effect)
      reportChange(effects)
      updateSubscriptions()
    },

    REMOVE_EFFECT: ({ effect }) => {
      effects.delete(effect)
      reportChange(effects)
      updateSubscriptions()
    },

    ...debug(log => {
      log(' ','hasEffects:', Array.from(effects))
    }),

    ...subscriptionHandlers,
  }
}

function cleanup(fn) {
  return {
    CLEANUP: fn
  }
}

function debug(fn) {
  return {
    DEBUG: (msg) => {
      const prefix = msg.prefix || ''

      function log(...args) {
        console.log(prefix, ...args)
      }

      function logActor(system, id) {
        system.send(id, {
          type: 'DEBUG',
          prefix: prefix + '  '
        })
      }

      fn(log, logActor)
    }
  }
}
