const FoodCreator = () => ({ system, id, world }) => {
  return handle({
    TICK: everyNTimes(1000, () => {
      const x = Math.floor(Math.random() * 40) - 20
      const y = Math.floor(Math.random() * 40) - 20
      system.send(world, {
        type: 'PLACE_ACTOR',
        position: `${x},${y}`,
        actor: system.addActor(Food()),
        details: { actorType: 'Food' },
      })
    })
  })
}

const Food = () => ({ system, id, gui }) => {
  let currentLocation = {}
  displayInGui(gui, id, 'Food')

  return composeHandlers(
    debug(log => {
      log('Food:')
      log(' ', 'id:', id)
    }),
    isVisible('Food', id, system),
    hasCurrentLocation(id, system, newLocation => { currentLocation = newLocation }),
    {
      EATEN_BY: ({ actor, position }) => {
        if(position === currentLocation.position) {
          system.send(actor, {
            type: 'FEED',
            amount: 100
          })
          system.removeActor(id)
        }
      }
    }
  )
}

const Hunger = (parentId, starvationThreshold, movementSpeed) => ({ system, id, world }) => {
  let hunger = 0
  let vision = null
  let currentLocation = {}
  let pathToFood = null

  const priority = sigmoidPriorityCurve(starvationThreshold)

  function foodAt(position) {
    if(!vision) return null
    if(!vision[position]) return null
    for(key in vision[position]) {
      if(vision[position][key].actorType === 'Food') return vision[position][key]
    }
    return null
  }

  function nextStep() {
    return currentLocation.position && pathToFood && pathToFood[0]
      ? pathToFood[0]
      : null
  }

  function findPathToFood() {
    if(!vision) return null
    if(!currentLocation.position) return null
    return findPath(currentLocation.position, position => !!foodAt(position))
  }

  function anyFoodInView() {
    if(!vision) return false
    return Object.keys(vision).some(foodAt)
  }

  function updateLocation(newLocation) {
    if(pathToFood && newLocation.position === pathToFood[0]) {
      pathToFood.shift()
    }
    currentLocation = newLocation
  }

  function updateVision(newVision) {
    vision = newVision
    if(pathToFood && !foodAt(pathToFood[pathToFood.length - 1])) {
      pathToFood = null
    }
  }

  const [move, mobilityHandlers] = isMobile(
    parentId,
    movementSpeed,
    system,
    world,
    () => currentLocation.position,
    failedMove => { if(pathToFood && pathToFood.includes(position)) pathToFood = null }
  )

  return composeHandlers(
    mobilityHandlers,
    subscribeTo('LOCATION', id, parentId, system, updateLocation),
    subscribeTo('VISION', id, parentId, system, updateVision),
    debug(log => {
      log('Hunger:')
      log(' ', 'for:', parentId)
      log(' ', 'hunger level:', hunger)
      log(' ', 'starvation at:', starvationThreshold)
      log(' ', 'current location:', currentLocation)
      log(' ', 'vision', vision)
      log(' ', 'path to food', pathToFood)
    }),
    {
      TICK: everyNTimes(50, () => {
        hunger++
        system.send(parentId, {
          type: 'UPDATE_PRIORITY',
          actor: id,
          priority: priority(hunger)
        })

        if(hunger >= starvationThreshold) {
          system.send(parentId, { type: 'DIE', cause: 'starvation' })
          system.removeActor(id)
          return
        }
      }),

      ACT: () => {
        if(currentLocation.position && foodAt(currentLocation.position)) {
          system.send(foodAt(currentLocation.position).actor, {
            type: 'EATEN_BY',
            actor: id,
            position: currentLocation.position,
          })
          return
        }

        if((!pathToFood || pathToFood.length === 0) && anyFoodInView()) {
          pathToFood = findPathToFood()
        }

        if(pathToFood) move(pathToFood[0])
      },

      FEED: ({ amount }) => {
        hunger -= amount
      },
    }
  )
}
