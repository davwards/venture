const Corpse = (ofWhom, cause) => ({ system, id, world, gui }) => {
  displayInGui(gui, id, 'Corpse', () => ({ cause }))

  return composeHandlers(
    debug(log => {
      log('Corpse:')
      log(' ', 'id:', id)
      log(' ', 'of:', ofWhom)
      log(' ', 'cause of death:', cause || 'unknown')
    }),
    isVisible('Corpse', id, system, () => ({of: ofWhom})),
  )
}

function isMortal(id, system) {
  let currentLocation = null

  return {
    DIE: ({ cause }) => {
      if(currentLocation) {
        system.send(currentLocation.space, {
          type: 'PLACE_ACTOR',
          position: currentLocation.position,
          actor: system.addActor(Corpse(id, cause))
        })
        system.send(currentLocation.space, {
          type: 'VACATE',
          actor: id
        })
      }
      system.removeActor(id)
    },
    ...subscribeTo('LOCATION', id, id, system, newLocation => {
      currentLocation = newLocation
    })
  }
}
