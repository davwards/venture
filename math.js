const sigmoidPriorityCurve = crisis => current => {
  const offset = crisis * 0.65
  const timescale = crisis / 10
  const scaledInput = (current - offset) / timescale

  // sigmoid curve ranging from 0 to 100, starts sloping up
  // about 50% of the way to crisis threshold
  return 50 * (scaledInput/Math.sqrt(1 + scaledInput**2) + 1)
}

function distance(p1,p2) {
  const [x1,y1] = coords(p1)
  const [x2,y2] = coords(p2)
  return Math.sqrt((x1-x2)**2 + (y1-y2)**2)
}

function neighboringPositions(position) {
  const [x,y] = coords(position)
  return [
    `${x},${y-1}`,
    `${x},${y+1}`,
    `${x+1},${y}`,
    `${x-1},${y}`,
    `${x+1},${y-1}`,
    `${x-1},${y+1}`,
    `${x+1},${y+1}`,
    `${x-1},${y-1}`,
  ]
}

const cachedCoords = {}

function coords(coordsString) {
  return cachedCoords[coordsString] = cachedCoords[coordsString] || coordsString.split(',').map(s => parseInt(s))
}

function relativePosition(positionA, positionB) {
  const [x1, y1] = coords(positionA)
  const [x2, y2] = coords(positionB)
  return `${x1-x2},${y1-y2}`
}

function translatePosition(positionA, positionB) {
  const [x1, y1] = coords(positionA)
  const [x2, y2] = coords(positionB)
  return `${x1+x2},${y1+y2}`
}

function invertTranslation(translation) {
  const [x,y] = coords(translation)
  return `${-x},${-y}`
}

function findPath(start, searchCondition) {
  let target = null
  let nodes = {}
  let frontier = {[start]: {from: null}}
  const nearestTarget = () => Object.keys(frontier).filter(searchCondition)[0]

  while(!(target = nearestTarget())) {
    nodes = {...nodes, ...frontier}
    if(Object.keys(nodes).length > 1e4) {
      return null
    }
    const newFrontier = {}
    Object.keys(frontier).forEach(position => {
      neighboringPositions(position).forEach(neighbor => {
        if(!nodes[neighbor]) newFrontier[neighbor] = { from: position }
      })
    })
    frontier = newFrontier
  }
  nodes = {...nodes, ...frontier}

  const path = [target]
  while(nodes[path[0]].from !== start && nodes[path[0]].from !== null) {
    path.unshift(nodes[path[0]].from)
  }

  return path
}

const lightSpread1 = (sx, sy, position) => {
  const [x, y] = coords(position)
  if(x === sx && y === sy) return neighboringPositions(position)

  if(x === sx && y > sy) return [
    `${x-1},${y}`,
    `${x+1},${y}`,
    `${x-1},${y+1}`,
    `${x},${y+1}`,
    `${x+1},${y+1}`,
  ]

  if(x === sx && y < sy) return [
    `${x-1},${y}`,
    `${x+1},${y}`,
    `${x-1},${y-1}`,
    `${x},${y-1}`,
    `${x+1},${y-1}`,
  ]

  if(x < sx && y === sy) return [
    `${x},${y-1}`,
    `${x},${y+1}`,
    `${x-1},${y-1}`,
    `${x-1},${y}`,
    `${x-1},${y+1}`,
  ]

  if(x > sx && y === sy) return [
    `${x},${y-1}`,
    `${x},${y+1}`,
    `${x+1},${y-1}`,
    `${x+1},${y}`,
    `${x+1},${y+1}`,
  ]

  if(x > sx && y > sy) return [
    `${x},${y+1}`,
    `${x+1},${y}`,
    `${x+1},${y+1}`,
  ]

  if(x < sx && y < sy) return [
    `${x},${y-1}`,
    `${x-1},${y}`,
    `${x-1},${y-1}`,
  ]

  if(x < sx && y > sy) return [
    `${x},${y+1}`,
    `${x-1},${y}`,
    `${x-1},${y+1}`,
  ]

  if(x > sx && y < sy) return [
    `${x},${y-1}`,
    `${x+1},${y}`,
    `${x+1},${y-1}`,
  ]
}

const lightSpread2 = (sx, sy, position) => {
  const [x, y] = coords(position)
  const neighbors = new Set(neighboringPositions(position))

  if(sx < x) {
    neighbors.delete(`${x-1},${y-1}`)
    neighbors.delete(`${x-1},${y}`)
    neighbors.delete(`${x-1},${y+1}`)
  }

  if(sx > x) {
    neighbors.delete(`${x+1},${y-1}`)
    neighbors.delete(`${x+1},${y}`)
    neighbors.delete(`${x+1},${y+1}`)
  }

  if(sy < y) {
    neighbors.delete(`${x-1},${y-1}`)
    neighbors.delete(`${x},${y-1}`)
    neighbors.delete(`${x+1},${y-1}`)
  }

  if(sy > y) {
    neighbors.delete(`${x-1},${y+1}`)
    neighbors.delete(`${x},${y+1}`)
    neighbors.delete(`${x+1},${y+1}`)
  }

  return neighbors
}
